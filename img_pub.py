#!/usr/bin/env python3

import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
from sensor_msgs.msg import Image

def img_pub():
    cap = cv2.VideoCapture(0)#打开摄像头
    cap.set(cv2.CAP_PROP_FRAME_WIDTH,640)#设置参数
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT,480)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
    
    # ROS节点初始化
    rospy.init_node('img_pub', anonymous=True)

	# 创建一个Publisher，队列长度10
    img_pub = rospy.Publisher('/img_data', Image, queue_size=10)

	#设置循环的频率
    rate = rospy.Rate(10)

    bridge=CvBridge()
    while not rospy.is_shutdown():
        flag, frame = cap.read()
        if not flag:
            rospy.loginfo("Camera break!")
            break
        
        data=bridge.cv2_to_imgmsg(frame,"bgr8")
        img_pub.publish(data) #发布转换好的图像类型消息
        rospy.loginfo("Publish img")
        # 按照循环频率延时
        rate.sleep()
if __name__ == '__main__':
    try:
        img_pub()
    except rospy.ROSInterruptException:
        pass