#!/usr/bin/env python3

import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
from sensor_msgs.msg import Image

class img_convert:
    def __init__(self):
        #创建一个Publisher，用于发布处理好的图像,队列长度10
        self.output=rospy.Publisher("/output", Image, queue_size=10)
        self.bridge=CvBridge()
        # 创建一个Subscriber，订阅名为/img_data的topic，注册回调函数personInfoCallback
        rospy.Subscriber("/img_data", Image, self.ImgInfoCallback)
    
    def ImgInfoCallback(self,data):
        
        #处理图像
        img=self.bridge.imgmsg_to_cv2(data,"bgr8")
        kernel=np.ones((5,5),np.uint8)
        hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)#BGR转为HSV
        lower_red=np.array([0,100,100])#红色阈值下界
        higher_red=np.array([10,200,200])#红色阈值上界
        mask=cv2.inRange(hsv,lower_red,higher_red)
        mask=cv2.medianBlur(mask,15)#中值滤波平滑红球
        mask=cv2.dilate(mask,kernel,iterations=1)
        
        ret,thresh=cv2.threshold(mask,127,255,cv2.THRESH_BINARY)#处理后图像变为二值图像
        contours,hierarchy=cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        
        #获得轮廓
        if  len(contours)!=0:     
            cnt=max(contours,key=cv2.contourArea)
            #cnt=contours[0]
            x,y,w,h=cv2.boundingRect(cnt)#轮廓分解
            rect=cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)#画出外接矩形
            font = cv2.FONT_HERSHEY_PLAIN
            text="("+str(x)+","+str(y)+")"
            cv2.putText(img, text,(x-40, y-5), font, 1, (0, 255, 0), 1)#标上坐标
        self.output.publish(self.bridge.cv2_to_imgmsg(img,"bgr8"))#发布处理好的图像
        rospy.loginfo("subscribe img")

if __name__ == '__main__':
    
    try:
        # ROS节点初始化
        rospy.init_node('img_sub', anonymous=True)
        img_convert()
        # 循环等待回调函数
        rospy.spin()
    except rospy.ROSInterruptException:
        pass